#Name: Sam LeCompte
#Date: 9/30/15 
#Project: Prime Numbers

def is_prime(num):

    if num < 2:
        print(num, "is not a valid integer. Please try again")
        return False
    else:
        for x in range(2, num):
            if(num % x == 0):
                return False

    return True

from random import randint
keepGoing = True 
while keepGoing:
  myNumber = (randint(0,100000))
  if(is_prime(myNumber)):
    print(myNumber, "is a prime number.")
  else:
    print(myNumber, "is not a prime number.")
  
  ask = input("Would you like to continue? Y/N ")
  if (ask == "N" or ask == "n"):
    keepGoing = False

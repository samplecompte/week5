#Name: Sam LeCompte
#Date: 9/30/15 
#Project: Prime Numbers

def is_prime(num):

    if num < 2:
        print(num, "is not a valid integer. Please try again")
        return False
    else:
        for x in range(2, num):
            if(num % x == 0):
                return False

    return True

for y in range(1,101):
  if(is_prime(y)):
    print(y, "is a prime number.")
  else:
    print(y, "is not a prime number.")
